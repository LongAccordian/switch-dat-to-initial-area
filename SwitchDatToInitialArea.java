import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Takes a No-Intro Switch dat fed to it and produces an Initial Area dat.
 *
 * @author LongAccordian
 *
 */
public final class SwitchDatToInitialArea {

    /**
     * Private constructor so this utility class cannot be instantiated.
     */
    private SwitchDatToInitialArea() {
    }

    /**
     * Parse the original header into a queue.
     *
     * @param in
     *            The original dat file we are parsing
     *
     * @return The parsed header from the original dat
     * @throws IOException
     */
    private static Queue<String> parseHeader(BufferedReader in)
            throws IOException {

        Queue<String> header = new LinkedList<>();
        String currLine = "";

        while (!currLine.contains("</header>")) {
            currLine = in.readLine();
            header.add(currLine);
        }

        return header;
    }

    /**
     * Parse games (w/ an Initial Area) into a queue.
     *
     * @param in
     *            The original dat file we are parsing
     *
     * @return The parsed games (w/ an Initial Area) from the original dat
     * @throws IOException
     */
    private static Queue<String> parseBody(BufferedReader in)
            throws IOException {

        Queue<String> body = new LinkedList<>();
        String currLine = "";

        // Parse until we reach the end of the dat
        while (!currLine.contains("</datafile>")) {

            currLine = in.readLine();

            // Parse the current entry
            if (currLine.contains("<game name=\"")) {

                boolean hasInitialArea = false;
                Queue<String> tmpEntry = new LinkedList<>();

                // Grab current entry
                while (!currLine.contains("</game>")) {
                    // Check if it has an Initial Area
                    if (currLine.contains("(Initial Area)")) {
                        hasInitialArea = true;
                    }
                    tmpEntry.add(currLine);
                    currLine = in.readLine();
                }
                tmpEntry.add(currLine);

                // If there was an initial area then add to body
                if (hasInitialArea) {
                    // Keep every line until the first game entry
                    String tmp = tmpEntry.remove();
                    while (!tmp.contains("<rom name=\"")) {
                        body.add(tmp);
                        tmp = tmpEntry.remove();
                    }
                    // Find and enqueue the initial area entry
                    while (!tmp.contains("</game>")) {
                        if (tmp.contains("(Initial Area)")) {
                            body.add(tmp);
                        }
                        tmp = tmpEntry.remove();
                    }
                    // End the entry by dequeueing </game>
                    body.add(tmp);
                }

            } else { // If it's not a game entry, then it's the end
                body.add(currLine);
            }
        }

        return body;
    }

    /**
     * Parse the original header into a queue.
     *
     * @param header
     *            The queued up original header
     * @param out
     *            The new datFile we are writing to
     * @param newFileName
     *            The filename of the dat we're writing to
     * @throws IOException
     */
    private static void writeHeader(Queue<String> header, BufferedWriter out,
            String newFileName) throws IOException {

        String currLine = "";
        while (!currLine.contains("<name>")) {
            currLine = header.remove();
            out.write(currLine + "\n");
        }

        // Get rid of the old description and put in our own
        header.remove();
        String newDescription = newFileName.substring(0,
                newFileName.lastIndexOf('(') - 1);
        out.write("\t\t<description>" + newDescription + "</description>\n");

        while (header.size() != 0) {
            currLine = header.remove();
            out.write(currLine + "\n");
        }

        return;
    }

    /**
     * Parse the body into the new dat.
     *
     * @param body
     *            The contents of the new dat
     * @param out
     *            The new dat we're writing to
     * @throws IOException
     */
    private static void writeBody(Queue<String> body, BufferedWriter out)
            throws IOException {

        while (body.size() != 0) {
            out.write(body.remove() + "\n");
        }

        return;
    }

    /**
     * Main method.
     *
     * @param args
     *            the command line arguments
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        String fileName = args[0];

        // Open file reader
        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader(fileName));
        } catch (IOException e) {
            System.out.println("Could not open file");
        }

        Queue<String> header = parseHeader(in);
        Queue<String> body = parseBody(in);
        in.close();

        String datName = fileName.substring(0, fileName.indexOf('(') - 1);
        String datMeta = fileName.substring(fileName.indexOf('('));
        String newFileName = datName + " (Initial Area) " + datMeta;
        // Open file writer
        BufferedWriter out = null;
        try {
            out = new BufferedWriter(new FileWriter(newFileName));
        } catch (IOException e) {
            System.out.println("Could not open file");
        }

        writeHeader(header, out, newFileName);
        writeBody(body, out);
        out.close();
    }
}